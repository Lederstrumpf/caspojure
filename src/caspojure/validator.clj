(ns caspojure.validator
  (:require [clojure.spec.gen.alpha :as gen]
            [clojure.spec.test.alpha :as stest]
            [clojure.spec.alpha :as s]
            [manifold.stream :as stream]
            [caspojure.message :as message]
            [caspojure.tools :as tools]
            ))

(s/def ::validator (s/keys :req [::message/relayer-id ::view]))
(s/def ::view (s/coll-of ::message/relay-message :gen-max 1))
(s/def ::valid-view (s/coll-of ::message/valid-message :gen-max 5))

(defn validator [relayer-id]
  {::validator (atom (gen/generate (s/gen ::validator
                                          {::message/relayer-id (fn [] (gen/return relayer-id))})))
   ::stream #(stream/stream)})

(def validators
  (into {}
       (map
        (fn [relayer-id] [relayer-id (validator relayer-id)])
        (range message/relayer-count))))

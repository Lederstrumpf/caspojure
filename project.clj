(defproject caspojure "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [
                 [org.clojure/clojure "1.10.0-alpha6"]
                 [org.clojure/test.check "0.10.0-alpha2" :scope "test"]
                 [manifold "0.1.8"]
                 [walmartlabs/datascope "0.1.1"]
                 [rhizome "0.2.9"]])

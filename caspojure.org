#+STARTUP: indent
#+STARTUP: inlineimages
* Motivation
Provide alternate implementation of Casper in Clojure.
* Leiningen
#+BEGIN_SRC clojure :tangle project.clj :eval no
(defproject caspojure "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [
                 [org.clojure/clojure "1.10.0-alpha6"]
                 [org.clojure/test.check "0.10.0-alpha2" :scope "test"]
                 [manifold "0.1.8"]
                 [walmartlabs/datascope "0.1.1"]
                 [rhizome "0.2.9"]])
#+END_SRC

* Validator implementation
** Namespace declaration
#+BEGIN_SRC clojure :tangle src/caspojure/message.clj
(ns caspojure.message
  (:require [clojure.spec.gen.alpha :as gen]
            [clojure.spec.test.alpha :as stest]
            [clojure.spec.alpha :as s]
            [caspojure.tools :as tools]
            [clojure.test.check.generators]
            ))
#+END_SRC

#+RESULTS:
: nil

#+BEGIN_SRC clojure :tangle src/caspojure/validator.clj
(ns caspojure.validator
  (:require [clojure.spec.gen.alpha :as gen]
            [clojure.spec.test.alpha :as stest]
            [clojure.spec.alpha :as s]
            [manifold.stream :as stream]
            [caspojure.message :as message]
            [caspojure.tools :as tools]
            ))
#+END_SRC

#+RESULTS:
: nil

#+BEGIN_SRC clojure :tangle src/caspojure/tools.clj
(ns caspojure.tools
  (:require [clojure.spec.gen.alpha :as gen]
            [clojure.spec.test.alpha :as stest]
            [clojure.spec.alpha :as s]
            [com.walmartlabs.datascope :as ds]
            [rhizome.viz :as viz]))
#+END_SRC

#+RESULTS:
: nil

*** Aside: Visualization of data
:PROPERTIES:
:header-args:clojure: :tangle src/caspojure/tools.clj :ns caspojure.tools
:END:
Pretty printing is of limited utility for complex data structures.
Instead we visualize using a modified version of ~com.walmartlabs.datascope/dot~.

#+BEGIN_SRC clojure
(defn dot
  "Given a root composite value, returns the DOT (Graphviz
  description) of the composite value."
  ([root-value]
   (dot root-value nil))
  ([root-value options]
   {:pre [(satisfies? ds/Composite root-value)]}
   (let [[{:keys [nodes edges]}] (ds/render-composite root-value {})
         options-string-fn #(apply str
                                   (interpose ", "
                                              (map (fn [[k v]] (str (name k) "=" v)) %)))
         font-options (options-string-fn (select-keys options [:fontname]))
         font (if (not= "" font-options) font-options "fontname=\"Hasklug Nerd Font Mono\"")
         root-options (options-string-fn (select-keys options [:bgcolor]))
         node-options (if options
                        (options-string-fn (dissoc options :bgcolor))
                        "shape=plaintext, style=\"rounded,filled\", fillcolor=\"#FAF0E6\"")]
     (with-out-str
       (println "digraph G {\n  rankdir=LR;")
       (println (if (not= "" root-options)
                  (str root-options ";")
                  "bgcolor=\"#FFFFFF00\";"))
       (println (str "graph [" font "];"))
       (println (str "node [" font "];"))
       (println (str "edge [" font "];"))
       (println (str "\n  node [" node-options "];"))

       (doseq [[id text] nodes]
         (println (str "  " id " [" text "];")))

       (println)

       (doseq [[from to] edges]
         (println (str "  " from " -> " to ";")))

       (println "}"))
     )))
#+END_SRC

#+RESULTS:
: #'caspojure.tools/dot

We also have some default options for the rendering of graphs
#+BEGIN_SRC clojure
(def graphviz-defaults {
                        :shape "plaintext"
                        :style "\"rounded,filled\""
                        ;; :color "\"#FFFFFF\""
                        :fontname "\"Hasklug Nerd Font Mono\""
                        :fontcolor "\"#FFFFFF00\""
                        :fillcolor "\"#555555\""
                        :bgcolor "\"#FFFFFF00\""
                        })
#+END_SRC

#+RESULTS:
: #'caspojure.tools/graphviz-defaults

Finally, we wrap this in a neat container.
#+BEGIN_SRC clojure
(defn data->svg
  ([data]
   (data->svg data graphviz-defaults))
  ([data options]
   (-> data
       (dot options)
       viz/dot->svg
       print)))
#+END_SRC

#+RESULTS:
: #'caspojure.tools/data->svg

#+BEGIN_SRC clojure :file svg/test.svg :results output :exports both :tangle no
(data->svg {:a 3 :b 2})
#+END_SRC

#+RESULTS:
[[file:svg/test.svg]]

** Messages
:PROPERTIES:
:header-args:clojure: :tangle src/caspojure/message.clj :ns caspojure.message
:END:
*** Basic justification
Each node maintains a current view of the world. Nodes send one another messages that contain their votes and use votes received from others as the justification.

For our immediate purposes, we cap the number of relayers at 10.
#+BEGIN_SRC clojure
(def relayer-count 10)
(s/def ::relayer-id (s/int-in 0 relayer-count))
#+END_SRC

#+RESULTS:
: #'caspojure.message/relayer-count
: :caspojure.message/relayer-id

Next, we define voting messages to consist of a relayer-id and a boolean vote value.
#+BEGIN_SRC clojure
(s/def ::vote boolean?)
(s/def ::vote-message (s/keys :req [::relayer-id ::vote]))
#+END_SRC

#+RESULTS:
: :caspojure.message/vote
: :caspojure.message/vote-message

An estimate either returns a boolean consensus value based on the currently available messages or ~nil~ when consensus could not be established (tie or genesis block).
#+BEGIN_SRC clojure
(s/def ::estimate (s/or :consensus boolean? :nil nil?))
#+END_SRC

#+RESULTS:
: :caspojure.message/estimate

A justification is built up from received and issued votes and past relay messages containing these. Relay messages have a recursive relationship with justifications.
#+BEGIN_SRC clojure
(s/def ::empty-vector (s/and vector? empty?))
(s/def ::justification (s/coll-of (s/or :relay ::relay-message :vote ::vote-message) :min-count 0 :gen-max 3))
(s/def ::relay-message (s/keys :req [::relayer-id ::estimate ::justification]))
(s/def ::genesis-message (s/spec (s/and
                                  (s/nonconforming ::relay-message)
                                  #(and (nil? (::estimate %)) (empty? (::justification %)))
                                  )

                                 :gen (fn [] (s/gen ::relay-message
                                                   {::estimate #(gen/return nil)
                                                    ::justification #(gen/return [])}))
                                 ))
#+END_SRC

#+RESULTS:
: :caspojure.message/empty-vector
: :caspojure.message/justification
: :caspojure.message/relay-message
: :caspojure.message/genesis-message

**** Examples
#+BEGIN_SRC clojure :tangle no
(s/explain ::justification [{::relayer-id 1 ::vote false}])
(s/explain ::justification [])

(gen/generate (s/gen ::relay-message))
(gen/generate (s/gen ::genesis-message))
(s/conform ::justification (gen/generate (s/gen ::justification)))
#+END_SRC

#+RESULTS:
: nil
: nil
: #:caspojure.message{:relayer-id 6, :estimate nil, :justification [#:caspojure.message{:relayer-id 7, :vote true}]}
: #:caspojure.message{:relayer-id 6, :estimate nil, :justification []}
: [#:caspojure.message{:relayer-id 5, :estimate false, :justification [#:caspojure.message{:relayer-id 7, :estimate nil, :justification [#:caspojure.message{:relayer-id 7, :estimate nil, :justification [#:caspojure.message{:relayer-id 2, :estimate true, :justification []} #:caspojure.message{:relayer-id 7, :estimate nil, :justification [#:caspojure.message{:relayer-id 7, :vote true} #:caspojure.message{:relayer-id 9, :vote true}]}]} #:caspojure.message{:relayer-id 6, :estimate nil, :justification [#:caspojure.message{:relayer-id 9, :estimate false, :justification [#:caspojure.message{:relayer-id 8, :vote false} #:caspojure.message{:relayer-id 7, :vote false}]}]}]}]}]

Here's an example of a message.
#+BEGIN_SRC clojure :tangle no
(s/valid? ::vote-message {::relayer-id 2 ::vote false})

(s/valid? ::relay-message {::relayer-id 1
                           ::estimate false
                           ::justification
                           [{::relayer-id 9
                             ::estimate true
                             ::justification [{::relayer-id 2
                                               ::estimate nil
                                               ::justification []}
                                              {::relayer-id 1
                                               ::estimate true
                                               ::justification [{::relayer-id 0
                                                                 ::estimate nil
                                                                 ::justification []}]}]}
                            {::relayer-id 0
                             ::estimate nil
                             ::justification []}]})
#+END_SRC

#+RESULTS:
: true
: true

Here's a message generated from its spec.
#+BEGIN_SRC clojure :tangle no
(gen/generate (s/gen ::vote-message))
#+END_SRC

#+RESULTS:
: #:caspojure.message{:relayer-id 6, :vote false}

#+BEGIN_SRC clojure :file svg/relay-message.svg :results output :exports both :tangle no
(caspojure.tools/data->svg (gen/generate (s/gen ::relay-message)))
#+END_SRC

#+RESULTS:
[[file:svg/relay-message.svg]]

Of course, not all justifications are mutually consistent, or even valid in their own right. Reasons for incompatibility/invalidity include distinct genesis blocks and equivocations.

Mutual consistence and internal consistence are tested the same - internal consistence requires a mutual genesis block and a lack of equivocations within a justification, whereas for mutual consistence, we first take the union of all justifications considered. 
*** Equivocations 
First, we need to gather all the votes found in a relay-message's justification
#+BEGIN_SRC clojure
(s/fdef votes-in-justification
        :args (s/cat :justification-or-vote (s/or :vote ::vote-message
                                                  :justification ::justification))
        :ret (s/coll-of ::vote-message))

(defn votes-in-justification
  [justification-or-vote]
   (cond
     (s/valid? ::vote-message justification-or-vote) [justification-or-vote]
     (s/valid? ::justification (::justification justification-or-vote)) (apply concat
                                                                               (map votes-in-justification
                                                                                    (::justification justification-or-vote)))
     true []
     ))
#+END_SRC

#+RESULTS:
: caspojure.message/votes-in-justification
: #'caspojure.message/votes-in-justification

Next, we filter these votes for equivocations, namely messages with contradictory votes from any validator.
#+BEGIN_SRC clojure
(s/fdef equivocations
        :args (s/cat :relay-message ::relay-message)
        :ret (s/coll-of (s/tuple ::relayer-id (s/coll-of ::vote-message))))

(defn equivocations [relay-message]
  (->> relay-message
       votes-in-justification
       (group-by ::relayer-id)
       (reduce-kv (fn [m k v] (assoc m k (into #{} v))) {})
       (filter #(< 1 (count (val %))))))
#+END_SRC

#+RESULTS:
: caspojure.message/equivocations
: #'caspojure.message/equivocations

**** Examples
#+BEGIN_SRC clojure :tangle no
(stest/check `votes-in-justification)
#+END_SRC

#+RESULTS:
: '((:spec #object(clojure.spec.alpha$fspec_impl$reify__2451 0x3f4b2b53 "clojure.spec.alpha$fspec_impl$reify__2451@3f4b2b53")  :clojure.spec.test.check/ret (:result true  :num-tests 1000  :seed 1527453271136)  :sym caspojure.message/votes-in-justification))


#+BEGIN_SRC clojure :tangle no
(votes-in-justification {::relayer-id 1
                         ::vote false})

(s/explain ::vote-message {::relayer-id 1
                           ::vote false})

(s/explain ::justification [{::relayer-id 1
                             ::estimate false
                             ::justification [{::relayer-id 0
                                               ::vote true}]}])

(votes-in-justification {::relayer-id 2
                         ::estimate true
                         ::justification [{::relayer-id 1
                                           ::estimate false

                                           ::justification [{::relayer-id 0
                                                             ::vote true}
                                                            {::relayer-id 1
                                                             ::vote false}]}
                                          {::relayer-id 2
                                           ::vote false}]})
#+END_SRC


#+RESULTS:
: [caspojure\.message{:relayer-id 1 (\, :vote) false}]

#+BEGIN_SRC clojure :tangle no
(stest/check `equivocations)
#+END_SRC

#+RESULTS:
: '((:spec #object(clojure.spec.alpha$fspec_impl$reify__2451 0x75f3f6b7 "clojure.spec.alpha$fspec_impl$reify__2451@75f3f6b7")  :clojure.spec.test.check/ret (:result true  :num-tests 1000  :seed 1527453271456)  :sym caspojure.message/equivocations))

#+BEGIN_SRC clojure :tangle no
(->> {::relayer-id 2
      ::estimate true
      ::justification [{::relayer-id 1
                        ::estimate false

                        ::justification [{::relayer-id 1
                                          ::vote true}
                                         {::relayer-id 1
                                          ::vote false}]}
                       {::relayer-id 2
                        ::vote false}]}
     votes-in-justification
     (group-by ::relayer-id)
     (reduce-kv (fn [m k v] (assoc m k (into #{} v))) {})
     (filter #(< 1 (count (val %)))))
#+END_SRC

#+RESULTS:
: '((1 #(#:caspojure.message(:relayer-id 1  :vote true) #:caspojure.message(:relayer-id 1  :vote false))))

From this, we specify that a valid justification should contain no equivocations.
#+BEGIN_SRC clojure
(s/def ::equivocation-void-message (s/and (s/nonconforming ::relay-message) #(-> % equivocations empty?)))
(s/def ::equivocation-full-message (s/and (s/nonconforming ::relay-message) #(->> % (s/valid? ::equivocation-void-message) not)))
#+END_SRC

#+RESULTS:
: :caspojure.message/equivocation-void-message
: :caspojure.message/equivocation-full-message

#+BEGIN_SRC clojure :tangle no
((juxt identity equivocations) (gen/generate (s/gen ::relay-message)))
((juxt identity equivocations) (gen/generate (s/gen ::equivocation-void-message)))
((juxt identity equivocations) (gen/generate (s/gen ::equivocation-full-message)))

(s/valid? ::equivocation-void-message {::relayer-id 2
                                       ::estimate true
                                       ::justification
                                       [{::relayer-id 1
                                         ::estimate false

                                         ::justification
                                         [{::relayer-id 0
                                           ::vote true} {::relayer-id 0
                                                         ::vote true} {::relayer-id 1
                                                                       ::vote false} {::relayer-id 9
                                                                                      ::estimate false
                                                                                      ::justification
                                                                                      [{::relayer-id 1
                                                                                        ::vote true} {::relayer-id 3
                                                                                                      ::vote false}]}]}]})

(equivocations {::relayer-id 2
                ::estimate true
                ::justification
                [{::relayer-id 1
                  ::estimate false
                  ::justification
                  [{::relayer-id 0
                    ::vote true} {::relayer-id 0
                                  ::vote true} {::relayer-id 1
                                                ::vote false} {::relayer-id 9
                                                               ::estimate false
                                                               ::justification
                                                               [{::relayer-id 1
                                                                 ::vote true} {::relayer-id 3
                                                                               ::vote false}]}]}]})
#+END_SRC

#+RESULTS:
: [#:caspojure.message{:relayer-id 6, :estimate true, :justification [#:caspojure.message{:relayer-id 5, :estimate true, :justification [#:caspojure.message{:relayer-id 8, :estimate nil, :justification [#:caspojure.message{:relayer-id 0, :estimate true, :justification [#:caspojure.message{:relayer-id 5, :estimate false, :justification []}]} #:caspojure.message{:relayer-id 5, :vote true} #:caspojure.message{:relayer-id 7, :vote false}]} #:caspojure.message{:relayer-id 5, :vote false}]} #:caspojure.message{:relayer-id 9, :vote true}]} ([5 #{#:caspojure.message{:relayer-id 5, :vote true} #:caspojure.message{:relayer-id 5, :vote false}}])]
: [#:caspojure.message{:relayer-id 6, :estimate true, :justification [#:caspojure.message{:relayer-id 8, :estimate false, :justification [#:caspojure.message{:relayer-id 6, :vote false} #:caspojure.message{:relayer-id 7, :estimate nil, :justification []} #:caspojure.message{:relayer-id 5, :estimate nil, :justification [#:caspojure.message{:relayer-id 9, :vote true}]}]} #:caspojure.message{:relayer-id 2, :vote false}]} ()]
: [#:caspojure.message{:relayer-id 8, :estimate false, :justification [#:caspojure.message{:relayer-id 8, :estimate true, :justification [#:caspojure.message{:relayer-id 7, :estimate nil, :justification [#:caspojure.message{:relayer-id 6, :vote true} #:caspojure.message{:relayer-id 5, :estimate false, :justification [#:caspojure.message{:relayer-id 6, :estimate true, :justification [#:caspojure.message{:relayer-id 6, :vote false} #:caspojure.message{:relayer-id 6, :vote false} #:caspojure.message{:relayer-id 9, :vote true}]} #:caspojure.message{:relayer-id 7, :vote false} #:caspojure.message{:relayer-id 0, :vote true}]}]} #:caspojure.message{:relayer-id 5, :estimate nil, :justification []} #:caspojure.message{:relayer-id 6, :vote true}]}]} ([6 #{#:caspojure.message{:relayer-id 6, :vote false} #:caspojure.message{:relayer-id 6, :vote true}}])]
: false
: ([1 #{#:caspojure.message{:relayer-id 1, :vote true} #:caspojure.message{:relayer-id 1, :vote false}}])

*** Estimators
In our binary vote system, an valid estimate of a justification is the majority result of the votes.
First, we must define the correct running result of a vote.
#+BEGIN_SRC clojure
(s/fdef estimate
        :args (s/cat :relay-message ::relay-message)
        :ret ::estimate)

(defn estimate [relay-message]
  (let [votes (votes-in-justification relay-message)
        counted-votes (->> votes
                           (into #{})
                           (group-by ::vote)
                           (reduce-kv (fn [m k v] (assoc m k (count v))) {}))]
    (case (count counted-votes)
      ;; no votes
      0 nil
      ;; unilateral consensus
      1 (-> counted-votes first key)
      ;; majority
      2 (case (apply compare (map val counted-votes))
          -1 (-> counted-votes second key)
          0 nil
          1 (-> counted-votes first key)
          ))))
#+END_SRC

#+RESULTS:
: caspojure.message/estimate
: #'caspojure.message/estimate

From this, we specify a valid justification.
#+BEGIN_SRC clojure
(s/def ::justified-message (s/spec (s/and (s/nonconforming ::relay-message) #(= (::estimate %) (estimate %)))

                                   :gen (fn [] (clojure.test.check.generators/let
                                                   [justification (s/gen ::justification
                                                                         {::relay-message #(s/gen ::justified-message)})
                                                    relayer-id (s/gen ::relayer-id)]
                                                 {::relayer-id relayer-id
                                                  ::estimate (estimate {::justification justification})
                                                  ::justification justification}))))
#+END_SRC

#+RESULTS:
: :caspojure.message/justified-message

**** Examples
#+BEGIN_SRC clojure :tangle no
(stest/check `estimate)
#+END_SRC

#+RESULTS:
: '((:spec #object(clojure.spec.alpha$fspec_impl$reify__2514 0x403a072b "clojure.spec.alpha$fspec_impl$reify__2514@403a072b")  :clojure.spec.test.check/ret (:result true  :num-tests 1000  :seed 1531934108849)  :sym caspojure.message/estimate))


#+BEGIN_SRC clojure :tangle no
(estimate {::relayer-id 0
           ::estimate false
           ::justification [{::relayer-id 2
                             ::vote false}
                            {::relayer-id 3
                             ::vote false}
                            {::relayer-id 1
                             ::vote true}]})

((juxt #(let [votes (votes-in-justification %)]
          (->> votes
               (into #{})
               (group-by ::vote)
               (reduce-kv (fn [m k v] (assoc m k (count v))) {})
               )
          ) estimate) (gen/generate (s/gen ::relay-message)))
#+END_SRC

#+RESULTS:
: true
: [{true 3, false 3} nil]

#+BEGIN_SRC clojure :tangle no
((juxt ::estimate estimate) (s/conform (s/nonconforming ::relay-message) (gen/generate (s/gen ::relay-message))))
((juxt votes-in-justification ::estimate estimate) (gen/generate (s/gen ::justified-message)))
#+END_SRC

#+RESULTS:
| true | nil |

*** Valid message
Having specified [[Equivocations][equivocations]] and [[Estimators][running vote results]], we can now combine these to provide the specification of a valid relay-message.
#+BEGIN_SRC clojure
(s/def ::valid-relay-message (s/and (s/nonconforming ::justified-message) ::equivocation-void-message))
#+END_SRC

#+RESULTS:
: :caspojure.message/valid-relay-message

#+BEGIN_SRC clojure
(s/def ::valid-message (s/or
                        :vote ::vote-message
                        :relay ::valid-relay-message
                        ))
#+END_SRC

#+RESULTS:
: :caspojure.message/valid-message

**** Examples

#+BEGIN_SRC clojure :tangle no
(gen/generate (s/gen ::valid-relay-message))
#+END_SRC

#+RESULTS:
: #:caspojure.message{:relayer-id 1, :estimate false, :justification [#:caspojure.message{:relayer-id 4, :estimate false, :justification [#:caspojure.message{:relayer-id 7, :vote false}]} #:caspojure.message{:relayer-id 7, :vote false} #:caspojure.message{:relayer-id 6, :vote false}]}

#+BEGIN_SRC clojure :tangle no
(gen/generate (s/gen ::justified-message))
#+END_SRC

#+RESULTS:
: #:caspojure.message{:relayer-id 4, :estimate nil, :justification [#:caspojure.message{:relayer-id 6, :vote false} #:caspojure.message{:relayer-id 7, :vote true}]}

#+BEGIN_SRC clojure :tangle no
(gen/generate (s/gen ::equivocation-void-message))
#+END_SRC

#+RESULTS:
: #:caspojure.message{:relayer-id 7, :estimate nil, :justification [#:caspojure.message{:relayer-id 1, :vote false} #:caspojure.message{:relayer-id 7, :vote true}]}

#+BEGIN_SRC clojure :file svg/valid-message.svg :results output :exports both :tangle no
(def valid-mess (gen/generate (s/gen ::valid-message)))
(tools/data->svg valid-mess)
#+END_SRC

#+RESULTS:
[[file:svg/valid-message.svg]]
#+BEGIN_SRC clojure :tangle no
(gen/generate (s/gen ::valid-message))
#+END_SRC

#+RESULTS:
: #:caspojure.message{:relayer-id 5, :estimate false, :justification [#:caspojure.message{:relayer-id 5, :estimate false, :justification [#:caspojure.message{:relayer-id 7, :vote false}]}]}

#+BEGIN_SRC clojure :tangle no
((juxt
  ::justification
  votes-in-justification
  #(s/valid? ::justified-message %)
  #(s/valid? ::equivocation-void-message %)
  identity)
 (gen/generate (s/gen ::valid-message)))

(s/explain ::justified-message {::relayer-id 1
                                ::estimate  true
                                ::justification []})

((juxt
  ::justification
  votes-in-justification
  #(s/valid? ::justified-message %)
  #(s/valid? ::equivocation-void-message %)
  identity)
 (gen/generate (s/gen ::valid-message)))

(s/explain ::justified-message {::relayer-id 1
                                ::estimate true
                                ::justification [{::relayer-id 1
                                                  ::vote false}
                                                 {::relayer-id 2
                                                  ::vote true}]})

(s/explain ::valid-message {::relayer-id 1
                            ::estimate true
                            ::justification [{::relayer-id 1
                                              ::vote false}
                                             {::relayer-id 1
                                              ::vote true}]})

(s/explain ::equivocation-void-message {::relayer-id 1
                                        ::estimate true
                                        ::justification [{::relayer-id 1
                                                          ::vote false}
                                                         {::relayer-id 1
                                                          ::vote true}]})
#+END_SRC

#+RESULTS:
: [nil [caspojure\.message{:relayer-id 6 (\, :vote) true}] false false caspojure\.message{:relayer-id 6 (\, :vote) true}]
** Validators
:PROPERTIES:
:header-args:clojure: :tangle src/caspojure/validator.clj :ns caspojure.validator
:END:
We now turn towards validators. Validators are identified with their relay-id. They maintain an internal view constructed from the messages they have received so far. Note that this internal view does not necessarily match the view a validator reveals in their broadcasted justifications - a validator can opt to omit particular knowledge in their justifications if this supports their adversarial ambitions. However, we may also construct dependent views that reflect a lack of trust in particular messages received, if a validator has reason to doubt their veracity.
#+BEGIN_SRC clojure
(s/def ::validator (s/keys :req [::message/relayer-id ::view]))
(s/def ::view (s/coll-of ::message/relay-message :gen-max 1))
(s/def ::valid-view (s/coll-of ::message/valid-message :gen-max 5))
#+END_SRC

#+RESULTS:
: :caspojure.validator/validator
: :caspojure.validator/view
: :caspojure.validator/valid-view

**** Examples
#+BEGIN_SRC clojure :tangle no
(gen/generate
 (s/gen :caspojure.message/relay-message {:caspojure.message/justification #(s/gen (s/coll-of (s/or :relay :caspojure.message/relay-message :vote :caspojure.message/vote-message) :min-count 0 :gen-max 5))}))
#+END_SRC

#+RESULTS:
: #:caspojure.message{:relayer-id 7, :estimate false, :justification [#:caspojure.message{:relayer-id 6, :estimate nil, :justification [#:caspojure.message{:relayer-id 7, :estimate nil, :justification [#:caspojure.message{:relayer-id 6, :vote false} #:caspojure.message{:relayer-id 6, :vote true} #:caspojure.message{:relayer-id 7, :vote false}]}]} #:caspojure.message{:relayer-id 6, :vote true} #:caspojure.message{:relayer-id 5, :estimate true, :justification [#:caspojure.message{:relayer-id 5, :estimate false, :justification [#:caspojure.message{:relayer-id 5, :estimate true, :justification []} #:caspojure.message{:relayer-id 7, :vote true}]} #:caspojure.message{:relayer-id 0, :estimate nil, :justification [#:caspojure.message{:relayer-id 5, :estimate nil, :justification []} #:caspojure.message{:relayer-id 9, :vote false}]} #:caspojure.message{:relayer-id 9, :estimate false, :justification [#:caspojure.message{:relayer-id 7, :vote true}]}]} #:caspojure.message{:relayer-id 5, :vote true}]}


#+BEGIN_SRC clojure :file svg/view-2.svg :results output :exports both :tangle no
;; (tools/data->svg (gen/generate (s/gen ::view
;;                                       {::message/justification
;;                                        #(s/gen
;;                                          (s/coll-of (s/or :relay :caspojure.message/relay-message :vote :caspojure.message/vote-message) :max-count 1))})))

(tools/data->svg (gen/generate (s/gen ::view
                                      {::justification #(clojure.test.check.generators/vector
                                                         (clojure.test.check.generators/frequency [[1 (s/gen :caspojure.message/relay-message)] [30 (s/gen :caspojure.message/vote-message)]]))})))
#+END_SRC

#+RESULTS:
[[file:svg/view-2.svg]]

Here is a view where justifications are valid all the way down. Note that we had to inject recursive generation of justified messages.
#+BEGIN_SRC clojure :file svg/view-2.svg :results output :exports both :tangle no
(tools/data->svg (gen/generate (s/gen ::valid-view
                                      {:caspojure.message/relay-message #(s/gen :caspojure.message/justified-message)})))
#+END_SRC

#+RESULTS:
[[file:svg/view-2.svg]]

If we had used the following definition of a justified message instead, we would clearly not have needed to inject recursive generation of valid messages. 
#+BEGIN_SRC clojure :ns caspojure.message :file svg/alternate-justification.svg :results output :tangle no
(s/def ::justification (s/coll-of (s/or :relay ::justified-message :vote ::vote-message) :min-count 0 :gen-max 3))
(s/def ::justified-message (s/and (s/nonconforming (s/keys :req [::relayer-id ::estimate ::justification]))
                                   ::equivocation-void-message
                                   #(= (::estimate %) (estimate %))
                                  ))

(tools/data->svg (gen/generate (s/gen ::justified-message)))
#+END_SRC

#+RESULTS:
[[file:svg/alternate-justification.svg]]


#+BEGIN_SRC clojure :file svg/validator.svg :results output :exports both :tangle no
(tools/data->svg (gen/generate (s/gen ::validator
                                      {::validator #(s/gen (s/keys :req [::message/relayer-id ::view]))})))
#+END_SRC

#+RESULTS:
[[file:svg/validator.svg]]

#+BEGIN_SRC clojure :file svg/view.svg :results output :exports both :tangle no
(tools/data->svg (gen/generate (s/gen ::message/valid-message)))
#+END_SRC

#+RESULTS:
[[file:svg/view.svg]]

*** Validator set
As mentioned [[Basic justification][earlier]], for now, we work with a fixed number of validators. We build a list of validators and their associated communication streams to facilitate simple communication between them.
#+BEGIN_SRC clojure
(defn validator [relayer-id]
  {::validator (atom (gen/generate (s/gen ::validator
                                          {::message/relayer-id (fn [] (gen/return relayer-id))})))
   ::stream #(stream/stream)})

(def validators
  (into {}
       (map
        (fn [relayer-id] [relayer-id (validator relayer-id)])
        (range message/relayer-count))))
#+END_SRC

#+RESULTS:
: #'caspojure.validator/validator
: #'caspojure.validator/validators
